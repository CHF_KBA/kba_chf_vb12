# KBA_CHF_VB12

Elearning course link: https://elearning.kba.ai/courses/course-v1:KBA+CHF_GO+2023_Q3/course/

Hyperledger Fabric readthedocs: https://hyperledger-fabric.readthedocs.io/en/release-2.5/index.html

Fabric samples: https://github.com/hyperledger/fabric-samples

Hyperledger Fabirc github: https://github.com/hyperledger/fabric

Hyperledger: https://www.hyperledger.org/

Hyperledger India Chapter: https://wiki.hyperledger.org/display/HIRC/Hyperledger+India+Regional+Chapter+Home

Hyperledger Fabric Certified Practitioner (HFCP): https://training.linuxfoundation.org/certification/hyperledger-fabric-certified-practitioner-hfcp/

Telegram link: https://t.me/+SoA6eKX1s8o2NmE1

Feedback link: https://forms.gle/aZwm3mSUZB4gQFhk6

Note: Project guidelines uploaded in Project_Guidelines folder


