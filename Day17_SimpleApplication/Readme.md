# Initializing Go project

```
go mod init simple-app
```

# Get all the packages
```
go mod tidy
```

# Running the go file
```
go run .
```

# json car data
```
{
	"carId": "car01",
	"make": "bmw",
	"model": "m4",
	"color": "red",
	"dateOfManufacture":"12/01/24",
	"manufacturerName": "fact01"
}

```