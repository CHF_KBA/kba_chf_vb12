# To Run this complete UI

### 1. Clone the repo

### 2. Change directory to Automobile-network
```
cd Automobile-network
```

### 3. Start the automobile network and deploy the chaincode using script file
```
chmod +x startAutomobileNetwork.sh
./startAutomobileNetwork.sh
```

### 4. Run the Auto-App 
```
go run .
```

### 5. To stop the Auto-App
```
press ctrl+c
```

### 6.To stop the automobile network using the script file
```
chmod +x stopAutomobileNetwork.sh
./stopAutomobileNetwork.sh
```