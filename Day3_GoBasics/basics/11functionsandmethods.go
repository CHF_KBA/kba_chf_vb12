package main

import "fmt"

type User struct{
	Name string
	Age int
	Email string
}

func main(){

	sum, double := add(245,5325213)
	fmt.Println(sum, double)
	user1 := User{"Tony", 25, "tony@example.com"}
	newAge := alterAgeFn(user1)
	fmt.Println(newAge)
	newAgeMethod := user1.AlterAgeMethod()
	fmt.Println(newAgeMethod)
	fmt.Println(user1.Age)
}

func add(a, b int) (int, int){
	return a+b, a*2
}

func alterAgeFn(user User) int{
	user.Age += 10
	return user.Age
}

func (u User) AlterAgeMethod() int {
	u.Age = u.Age + 10
	return u.Age
}

func (u *User) AlterAgePtr() int {
	u.Age += 5
	return u.Age
}




